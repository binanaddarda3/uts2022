<!DOCTYPE html>
<html>

<head>
    <title>Nomor 1</title>
</head>
<body>
    <div class="container">
        <h2>Form Pendaftaran</h2>
        <form action="simpan-pendaftaran.php" method="post">
            <div class="form-group">
                <label>Username:</label>
                <input type="text" name="username" class="form-control" placeholder="Masukan Username" />
            </div><br>

            <div class="form-group">
                <label>Nama:</label>
                <input type="text" name="nama" class="form-control" placeholder="Masukan Nama" />
            </div><br>

            <div class="form-group">
                <label>Alamat:</label>
                <textarea name="alamat" class="form-control" rows="5" placeholder="Masukan Alamat"></textarea>
            </div><br>

            <div class="form-group">
                <label>Email:</label>
                <input type="email" name="email" class="form-control" placeholder="Masukan Email" />
            </div><br>

            <div class="form-group">
                <label>No HP:</label>
                <input type="text" name="no_hp" class="form-control" placeholder="Masukan No HP" />
            </div><br>

            <div class="form-group">
                <label>Password:</label>
                <input type="password" name="password" class="form-control" placeholder="Masukan Password" />
            </div><br>

            <button type="submit" name="submit" class="btn btn-primary">Submit</button>

        </form>
    </div>
</body>

</html>